# portal_spolecznosciowy
Zainstalowane składniki:
Wykorzystana zewnętrzna baza danych postgresql
Django	3.2.3	3.2.3
Pillow	8.2.0	8.2.0
PyJWT	2.1.0	2.1.0
asgiref	3.3.4	3.3.4
certifi	2021.5.30	2021.5.30
cffi	1.14.5	1.14.5
chardet	4.0.0	4.0.0
cryptography	3.4.7	3.4.7
defusedxml	0.7.1	0.7.1
idna	2.10	3.2
oauthlib	3.1.1	3.1.1
pip	21.1.2	21.1.2
psycopg2	2.8.6	2.8.6
pycparser	2.20	2.20
python3-openid	3.2.0	3.2.0
pytz	2021.1	2021.1
requests	2.25.1	2.25.1
requests-oauthlib	1.3.0	1.3.0
setuptools	57.0.0	57.0.0
six	1.16.0	1.16.0
social-auth-app-django	4.0.0	4.0.0
social-auth-core	4.1.0	4.1.0
sorl-thumbnail	12.7.0	12.7.0
sqlparse	0.4.1	0.4.1
urllib3	1.26.5	1.26.5

